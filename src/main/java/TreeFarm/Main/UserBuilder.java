package TreeFarm.Main;

import TreeFarm.MainApp.model.User;

public class UserBuilder {
    private User myUser;

    public void buildUser(String firstName, String lastName, String username, String email, String phoneNumber, String type, String password) {
        myUser = new User();
        myUser.setFirstName(firstName);
        myUser.setLastName(lastName);
        myUser.setUsername(username);
        myUser.setEmail(email);
        myUser.setPhoneNumber(phoneNumber);
        myUser.setUser_type(type);
        myUser.setPassword(password);
    }
    public User getUser(){
        return myUser;
    }
}
