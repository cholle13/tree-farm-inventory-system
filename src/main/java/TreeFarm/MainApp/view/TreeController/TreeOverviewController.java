/**
 * author: Chris Holle
 */
package TreeFarm.MainApp.view.TreeController;

import TreeFarm.Database.SQLStuff;
import TreeFarm.Main.Manager;
import TreeFarm.MainApp.model.Tree;
import TreeFarm.MainApp.ShowWindows;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import java.util.Formatter;


public class TreeOverviewController {
    @FXML
    private TableView<Tree> treeTable;
    @FXML
    private TableColumn<Tree, String> treeNameColumn;
    @FXML
    private TableColumn<Tree, Integer> treeSizeColumn;
    @FXML
    private Label typeNameLabel;
    @FXML
    private Label treeNameLabel;
    @FXML
    private Label sizeLabel;
    @FXML
    private Label ageLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private Label priceLabel;
    @FXML
    private Label originLabel;
    @FXML
    private Label quantityLable;

    private ShowWindows mainApp;

    public TreeOverviewController() {}

    @FXML
    private void initialize() {
        //init the tree table with two columns
        treeNameColumn.setCellValueFactory(treeNameColumn -> treeNameColumn.getValue().nameProperty());
        treeSizeColumn.setCellValueFactory(treeSizeColumn -> treeSizeColumn.getValue().sizeProperty().asObject());

        // Clear Tree details
        showTreeDetails(null);

        // Listen for selection changes and show person details when changed
        treeTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showTreeDetails(newValue));
    }

    public void setMainApp(ShowWindows mainApp) {
        this.mainApp = mainApp;
        treeTable.setItems(mainApp.getTreeData());
    }

    private void showTreeDetails(Tree tree) {
        if(tree != null){
            // Fill the labels with info from the tree object
            Formatter fmt = new Formatter();
            typeNameLabel.setText(tree.getType());
            treeNameLabel.setText(tree.getName());
            sizeLabel.setText(String.valueOf(tree.getSize()));
            ageLabel.setText(String.valueOf(tree.getAge()));
            descriptionLabel.setText(tree.getDescription());
            priceLabel.setText(String.valueOf(fmt.format("%.2f", tree.getPrice())));
            quantityLable.setText(String.valueOf(tree.getQuantity()));
            originLabel.setText(tree.getOrigin());
        }else {
            // if none selected, remove text
            typeNameLabel.setText("");
            treeNameLabel.setText("");
            sizeLabel.setText("");
            ageLabel.setText("");
            descriptionLabel.setText("");
            priceLabel.setText("");
            quantityLable.setText("");
            originLabel.setText("");
        }
    }

    private void handleNoSelectedTree() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.initOwner(mainApp.getPrimaryStage());
        alert.setTitle("No Tree Selected");
        alert.setHeaderText("Select a tree");
        alert.setContentText("Please select a tree!");
        alert.showAndWait();
    }

    // This deletes the tree altogether
    @FXML
    private void handleSellTree() {
        Tree selectedTree = treeTable.getSelectionModel().getSelectedItem();
        if(selectedTree == null){
            handleNoSelectedTree();
        }else {
            boolean okClicked = mainApp.showTreeSellDialog(selectedTree);
            if(okClicked){
                SQLStuff.buyTrees(Manager.connection.getConnection(), selectedTree, selectedTree.getQuantity());
            }
            showTreeDetails(selectedTree);
        }
    }

    // opens dialogue box and retrieves the edited tree
    @FXML
    private void handleBuyTree() {
        Tree selectedTree = treeTable.getSelectionModel().getSelectedItem();
        if(selectedTree == null){
            handleNoSelectedTree();
        }else {
            boolean okClicked = mainApp.showTreeBuyDialog(selectedTree);
            if(okClicked){
                SQLStuff.buyTrees(Manager.connection.getConnection(), selectedTree, selectedTree.getQuantity());
            }
            showTreeDetails(selectedTree);
        }
    }

    @FXML
    private void handleEditTree() {
        Tree selectedTree = treeTable.getSelectionModel().getSelectedItem();
        if(selectedTree == null){
            handleNoSelectedTree();
        } else {
            Tree oldTree = new Tree(selectedTree);
            boolean okClicked = mainApp.showTreeEditDialog(selectedTree);
            if(okClicked){
                SQLStuff.editTree(Manager.connection.getConnection(), selectedTree, oldTree);
            }
            setMainApp(mainApp);
            showTreeDetails(selectedTree);
        }

    }


}
