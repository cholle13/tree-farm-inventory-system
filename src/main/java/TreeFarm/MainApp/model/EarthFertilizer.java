package TreeFarm.MainApp.model;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class EarthFertilizer extends Fertilizer {
    public EarthFertilizer() {
        // ("Rock Phosphate", "Helps the roots", 16, 3, 23.95)
        this.name = new SimpleStringProperty("Rock Phosphate");
        this.description = new SimpleStringProperty("Helps the roots");
        this.size = new SimpleIntegerProperty(16);
        this.quantity = new SimpleIntegerProperty(3);
        this.price = new SimpleDoubleProperty(23.95);
    }
}
