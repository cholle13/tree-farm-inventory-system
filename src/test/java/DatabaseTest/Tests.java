/*
 * Author: Wyatt Snyder
 */

package DatabaseTest;

import TreeFarm.Database.ConnectionSingleton;
import TreeFarm.Database.SQLStuff;
import TreeFarm.MainApp.model.Fertilizer;
import TreeFarm.MainApp.model.Tree;
import TreeFarm.MainApp.model.User;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Tests {
    static private ConnectionSingleton connection = ConnectionSingleton.getDBConnection();

    @org.junit.jupiter.api.BeforeEach
    void init(){
        synchronized (Tests.class){
            SQLStuff.dropDBTable(connection.getConnection(), "Trees");
            SQLStuff.dropDBTable(connection.getConnection(), "Fertilizer");
            SQLStuff.dropDBTable(connection.getConnection(), "Users");
            SQLStuff.createDBFertilizerTable(connection.getConnection());
            SQLStuff.createDBTreesTable(connection.getConnection());
            SQLStuff.createDBUsersTable(connection.getConnection());
        }
    }

    @org.junit.jupiter.api.AfterAll
    static void cleanUp(){
        SQLStuff.dropDBTable(connection.getConnection(), "Trees");
        SQLStuff.dropDBTable(connection.getConnection(), "Fertilizer");
        SQLStuff.dropDBTable(connection.getConnection(), "Users");
        SQLStuff.createDBFertilizerTable(connection.getConnection());
        SQLStuff.createDBTreesTable(connection.getConnection());
        SQLStuff.createDBUsersTable(connection.getConnection());
    }


    @Test
    void insertTrees(){
        synchronized (Tests.class){
            Tree tree = new Tree("myTree", 4,5,15.99,"treetree",
                    "short little guy", 4, "earth");

            SQLStuff.insertIntoTreeTable(connection.getConnection(), tree);
            ObservableList<Tree> list = SQLStuff.getTrees(connection.getConnection());
            list.get(0).setPrice(Math.round(list.get(0).getPrice() * 100.0) / 100.0);
            Assertions.assertTrue(list.get(0).getName().equals(tree.getName()));
            Assertions.assertTrue(list.get(0).getType().equals(tree.getType()));
            Assertions.assertTrue(list.get(0).getDescription().equals(tree.getDescription()));
            Assertions.assertTrue(list.get(0).getOrigin().equals(tree.getOrigin()));
            Assertions.assertTrue(list.get(0).getAge() == tree.getAge());
            Assertions.assertTrue(list.get(0).getSize() == tree.getSize());
            Assertions.assertTrue(list.get(0).getPrice() == tree.getPrice());
            Assertions.assertTrue(list.get(0).getQuantity() == tree.getQuantity());
        }
    }

    @Test
    void buyTrees() {
        synchronized (Tests.class){
            Tree tree = new Tree("myTree", 4,5,15.99,"treetree",
                    "short little guy", 4, "earth");

            SQLStuff.insertIntoTreeTable(connection.getConnection(), tree);

            SQLStuff.buyTrees(connection.getConnection(), tree, 7);

            ObservableList<Tree> list = SQLStuff.getTrees(connection.getConnection());

            Assertions.assertTrue(list.get(0).getQuantity() == 7);
        }
    }

    @Test
    void editTree() {
        synchronized (Tests.class){
            Tree tree = new Tree("myTree", 4,5,15.99,"treetree",
                    "short little guy", 4, "earth");
            Tree newTree = new Tree("littTree", 4, 5, 13.50, "treetreetree",
                    "littleguy", 4, "earth");

            SQLStuff.insertIntoTreeTable(connection.getConnection(), tree);

            SQLStuff.editTree(connection.getConnection(), newTree, tree);

            ObservableList<Tree> list = SQLStuff.getTrees(connection.getConnection());
            list.get(0).setPrice(Math.round(list.get(0).getPrice() * 100.0) / 100.0);
            Assertions.assertTrue(list.get(0).getName().equals(newTree.getName()));
            Assertions.assertTrue(list.get(0).getType().equals(newTree.getType()));
            Assertions.assertTrue(list.get(0).getDescription().equals(newTree.getDescription()));
            Assertions.assertTrue(list.get(0).getOrigin().equals(newTree.getOrigin()));
            Assertions.assertTrue(list.get(0).getAge() == newTree.getAge());
            Assertions.assertTrue(list.get(0).getSize() == newTree.getSize());
            Assertions.assertTrue(list.get(0).getPrice() == newTree.getPrice());
            Assertions.assertTrue(list.get(0).getQuantity() == newTree.getQuantity());
        }
    }

    @Test
    void insertFertilizer() {
        synchronized (Tests.class){
            Fertilizer fert = new Fertilizer("myFert", 5,
                    5.99, "kool Fert", 4);

            SQLStuff.insertIntoFertilizerTable(connection.getConnection(), fert);
            ObservableList<Fertilizer> list = SQLStuff.getFertilizer(connection.getConnection());
            list.get(0).setPrice(Math.round(list.get(0).getPrice() * 100.0) / 100.0);
            Assertions.assertTrue(list.get(0).getName().equals(fert.getName()));
            Assertions.assertTrue(list.get(0).getDescription().equals(fert.getDescription()));
            Assertions.assertTrue(list.get(0).getSize() == fert.getSize());
            Assertions.assertTrue(list.get(0).getPrice() == fert.getPrice());
            Assertions.assertTrue(list.get(0).getQuantity() == fert.getQuantity());
        }
    }

    @Test
    void buyFertilizer() {
        synchronized (Tests.class){
            Fertilizer fert = new Fertilizer("myFert", 5,
                    5.99, "kool Fert", 4);

            SQLStuff.insertIntoFertilizerTable(connection.getConnection(), fert);

            SQLStuff.buyFertilizer(connection.getConnection(), fert, 7);

            ObservableList<Fertilizer> list = SQLStuff.getFertilizer(connection.getConnection());

            Assertions.assertEquals(7, list.get(0).getQuantity());
        }
    }

    @Test
    void createUser() {
        synchronized (Tests.class){
            User user = new User("bob", "baylor", "myusername", "koolEmail@baylor.edu",
                                "123456", "employee", "pass1234");

            SQLStuff.createUser(connection.getConnection(), user);
            ObservableList<User> list = SQLStuff.getUsers(connection.getConnection());

            Assertions.assertTrue(list.get(0).getFirstName().equals(user.getFirstName()));
            Assertions.assertTrue(list.get(0).getLastName().equals(user.getLastName()));
            Assertions.assertTrue(list.get(0).getUsername().equals(user.getUsername()));
            Assertions.assertTrue(list.get(0).getEmail().equals(user.getEmail()));
            Assertions.assertTrue(list.get(0).getPhoneNumber().equals(user.getPhoneNumber()));
            Assertions.assertTrue(list.get(0).getUser_type().equals(user.getUser_type()));
            Assertions.assertTrue(list.get(0).getPassword().equals(user.getPassword()));
        }
    }

}

