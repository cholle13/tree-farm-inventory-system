/**
 * author: Chris Holle
 */
package TreeFarm.MainApp.view.UserController;

import TreeFarm.MainApp.model.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class EditUserDialogController {
    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField username;
    @FXML
    private TextField email;
    @FXML
    private TextField phoneNumber;
    @FXML
    private ChoiceBox<String> choiceBox;

    private Stage dialogStage;
    private User user;
    private boolean okClicked = false;

    /**
     * isOkClicked
     * returns boolean variable
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * setDialogStage
     * Sets stage for modal windows
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * setUser
     * inserts the user data into to the input field to be edited
     * @param user
     */
    public void setUser(User user){
        this.user = user;
        firstName.setText(user.getFirstName());
        lastName.setText(user.getLastName());
        username.setText(user.getUsername());
        email.setText(user.getEmail());
        phoneNumber.setText(user.getPhoneNumber());
        choiceBox.setValue(user.getUser_type());
    }

    /**
     * initialize
     * Sets up our choice box to limit users
     * on choosing user type of admin or employee
     */
    public void initialize(){
        ObservableList<String> list = FXCollections.observableArrayList();
        list.addAll("employee", "admin");
        //populate the Choicebox;
        choiceBox.setItems(list);
    }

    /**
     * handleCancel
     * Adds close window functionality to cancel button
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * handleOk
     * handler for the ok button in the edit dialog box.
     * Sets user to the admin inputted data
     */
    @FXML
    private void handleOk() {
        if(isInputValid()){
            user.setFirstName(firstName.getText());
            user.setLastName(lastName.getText());
            user.setUsername(username.getText());
            user.setEmail(email.getText());
            user.setPhoneNumber(phoneNumber.getText());
            user.setUser_type(choiceBox.getValue());

            okClicked = true;
            dialogStage.close();
            initialize();
        }
    }

    /**
     * isInputValid
     * Verifies that user is entering correct data.
     * Adds first layer of security against ID10T Errors and SQL Injection
     * @return
     */
    private boolean isInputValid() {
        String errMssg = "";
        if(firstName.getText() == null || firstName.getText().length() == 0)
            errMssg += "No first name entered!\n";
        if(!(firstName.getText().matches("[a-zA-Z]+")))
            errMssg += "First Name field should only contain alpha characters\n";

        if(lastName.getText() == null || lastName.getText().length() == 0)
            errMssg += "No last name entered!\n";
        if(!(lastName.getText().matches("[a-zA-Z]+")))
            errMssg += "Last Name field should only contain alpha characters\n";

        if(username.getText() == null || username.getText().length() == 0)
            errMssg += "No username entered!\n";
        if(!(username.getText().matches("[a-z._A-Z0-9]+")))
            errMssg += "username field should only contain alpha characters, numerical digits, a period or _\n";

        if(email.getText() == null || email.getText().length() == 0)
            errMssg += "No email entered!\n";
        if(!(email.getText().matches("[a-z@.A-Z]+")))
            errMssg += "email field should only contain alpha characters, @ and a .\n";

        if(phoneNumber.getText() == null || phoneNumber.getText().length() == 0)
            errMssg += "No phone number entered!\n";

        if(!(phoneNumber.getText().matches("[0-9]{3}[-]{1}[0-9]{3}[-]{1}[0-9]{4}")))
            errMssg += "Phone number field should be in the format ###-###-####\n";
        if(choiceBox.getValue() == null || choiceBox.getValue().length() == 0){
            errMssg += "No user type entered!\n";
        }

        if(errMssg.length() == 0)
            return true;
        else{
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please fix the invalid fields");
            alert.setContentText(errMssg);

            alert.showAndWait();
            return false;
        }
    }

}
