/**
 * author: Chris Holle
 */
package TreeFarm.MainApp.view.MenuController;

import TreeFarm.Main.Manager;
import TreeFarm.MainApp.ShowWindows;
import javafx.fxml.FXML;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class ImportUsersController {

    private ShowWindows mainApp;
    private Stage dialogStage;
    private boolean okClicked = false;

    public void setMainApp(ShowWindows mainApp) {
        this.mainApp = mainApp;
    }
    public boolean isOkClicked() {
        return okClicked;
    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    @FXML
    private void handleOpen() {
        FileChooser fileChooser = new FileChooser();

        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files(*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);

        File file = fileChooser.showOpenDialog(mainApp.getPrimaryStage());
        if(file != null){
            Manager.importUsers(file, Manager.connection.getConnection());
            handleCancel();
        }

    }
}
