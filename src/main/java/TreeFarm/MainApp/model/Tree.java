/**
 * author: Chris Holle, Wyatt Snyder
 */
package TreeFarm.MainApp.model;

import javafx.beans.property.*;

public class Tree {
    protected StringProperty type;
    protected IntegerProperty age;
    protected IntegerProperty size;
    protected StringProperty origin;
    protected IntegerProperty quantity;
    protected DoubleProperty price;
    protected StringProperty name;
    protected StringProperty description;
    
    public Tree(String type, int age, int size, double price,
                String name, String description, int quantity, String origin){
        this.type = new SimpleStringProperty(type);
        this.age = new SimpleIntegerProperty(age);
        this.size = new SimpleIntegerProperty(size);
        this.price = new SimpleDoubleProperty(price);
        this.name = new SimpleStringProperty(name);
        this.description = new SimpleStringProperty(description);
        this.origin = new SimpleStringProperty(origin);
        this.quantity = new SimpleIntegerProperty(quantity);
    }
    public Tree(Tree that){
        this.type = new SimpleStringProperty(that.getType());
        this.age = new SimpleIntegerProperty(that.getAge());
        this.size = new SimpleIntegerProperty(that.getSize());
        this.price = new SimpleDoubleProperty(that.getPrice());
        this.name = new SimpleStringProperty(that.getName());
        this.description = new SimpleStringProperty(that.getDescription());
        this.origin = new SimpleStringProperty(that.getOrigin());
        this.quantity = new SimpleIntegerProperty(that.getQuantity());
    }

    public Tree() {

    }

    public String getType() {
        return type.get();
    }

    public StringProperty typeProperty() {
        return type;
    }

    public void setType(String type) {
        this.type.set(type);
    }

    public int getAge() {
        return age.get();
    }

    public IntegerProperty ageProperty() {
        return age;
    }

    public void setAge(int age) {
        this.age.set(age);
    }

    public int getSize() {
        return size.get();
    }

    public IntegerProperty sizeProperty() {
        return size;
    }

    public void setSize(int size) {
        this.size.set(size);
    }

    public double getPrice() {
        return price.get();
    }

    public DoubleProperty priceProperty() {
        return price;
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getDescription() {
        return description.get();
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public String getOrigin() {
        return origin.get();
    }

    public StringProperty originProperty() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin.set(origin);
    }

    public int getQuantity() {
        return quantity.get();
    }

    public IntegerProperty quantityProperty() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity.set(quantity);
    }


}
