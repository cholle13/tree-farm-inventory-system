/**
 * author: Chris Holle
 */
package TreeFarm.MainApp.view.FertilizerController;

import TreeFarm.MainApp.model.Fertilizer;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.Formatter;

public class FertilizerEditDialogController {
    @FXML
    private TextField nameLabel;
    @FXML
    private TextField sizeLabel;
    @FXML
    private TextArea descriptionLabel;
    @FXML
    private TextField priceLabel;

    private Stage dialogStage;
    private Fertilizer fertilizer;
    private boolean okClicked = false;

    public boolean isOkClicked() {
        return okClicked;
    }
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setFertilizer(Fertilizer fert){
        Formatter fmt = new Formatter();
        this.fertilizer = fert;
        nameLabel.setText(fertilizer.getName());
        sizeLabel.setText(String.valueOf(fertilizer.getSize()));
        priceLabel.setText(String.valueOf(fmt.format("%.2f", fertilizer.getPrice())));
        descriptionLabel.setText(fertilizer.getDescription());
    }

    public void initialize(){}

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    @FXML
    private void handleOk() {
        if(isInputValid()){
            fertilizer.setName(nameLabel.getText());
            fertilizer.setSize(Integer.parseInt(sizeLabel.getText()));
            fertilizer.setPrice(Double.parseDouble(priceLabel.getText()));
            fertilizer.setDescription(descriptionLabel.getText());

            okClicked = true;
            dialogStage.close();
            initialize();
        }
    }

    private boolean isInputValid() {
        String errMssg = "";
        if(nameLabel.getText() == null || nameLabel.getText().length() == 0){
            errMssg += "No fertilizer name entered!\n";
        }
        if(!(nameLabel.getText().matches("[a-z A-Z]+")))
            errMssg += "Name should only contain alpha characters!\n";
        if(descriptionLabel.getText() == null || descriptionLabel.getText().length() == 0){
            errMssg += "No description entered!\n";
        }
        if(!(descriptionLabel.getText().matches("[a-z A-Z.,!?]+")))
            errMssg += "Description should only contain alpha characters!\n";
        if(sizeLabel.getText() == null || sizeLabel.getText().length() == 0){
            errMssg += "No size entered!\n";
        }
        if(!(sizeLabel.getText().matches("[0-9]+")))
            errMssg += "Size should only contain numerical digits!\n";
        if(priceLabel.getText() == null || priceLabel.getText().length() == 0){
            errMssg += "No price entered!\n";
        }
        if(!(priceLabel.getText().matches("[0-9]+[.]{1}[0-9]+")))
            errMssg += "Name should only contain numerical digits!\n";

        if(errMssg.length() == 0)
            return true;
        else{
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields!");
            alert.setHeaderText("Please correct the invalid fields");
            alert.setContentText(errMssg);
            alert.showAndWait();
            return false;
        }
    }

}
