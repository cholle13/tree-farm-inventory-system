/**
 * author: Chris Holle, Wyatt Snyder
 */
package TreeFarm.MainApp.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Objects;

public class User {
    protected StringProperty firstName;
    protected StringProperty lastName;
    protected StringProperty username;
    protected StringProperty email;
    protected StringProperty phoneNumber;
    protected StringProperty user_type;
    protected StringProperty password;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(username, user.username) &&
                Objects.equals(email, user.email) &&
                Objects.equals(phoneNumber, user.phoneNumber) &&
                Objects.equals(user_type, user.user_type) &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {

        return Objects.hash(firstName, lastName, username, email, phoneNumber, user_type, password);
    }

    public User(User that){
        this.firstName = new SimpleStringProperty(that.getFirstName());
        this.lastName = new SimpleStringProperty(that.getLastName());
        this.username = new SimpleStringProperty(that.getUsername());
        this.email = new SimpleStringProperty(that.getEmail());
        this.phoneNumber = new SimpleStringProperty(that.getPhoneNumber());
        this.user_type = new SimpleStringProperty(that.getUser_type());
        this.password = new SimpleStringProperty(that.getPassword());
    }

    public User(){
        this.firstName = new SimpleStringProperty("");
        this.lastName = new SimpleStringProperty("");
        this.username = new SimpleStringProperty("");
        this.email = new SimpleStringProperty("");
        this.phoneNumber = new SimpleStringProperty("");
        this.user_type = new SimpleStringProperty("");
        this.password = new SimpleStringProperty("");
    }
    public User(String fname, String lname, String uname, String email, String phoneNumber, String user_type, String Password){
        this.firstName = new SimpleStringProperty(fname);
        this.lastName = new SimpleStringProperty(lname);
        this.username = new SimpleStringProperty(uname);
        this.email = new SimpleStringProperty(email);
        this.phoneNumber = new SimpleStringProperty(phoneNumber);
        this.user_type = new SimpleStringProperty(user_type);
        this.password = new SimpleStringProperty(Password);
    }

    public String getFirstName() {
        return firstName.get();
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getLastName() {
        return lastName.get();
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public String getUsername() {
        return username.get();
    }

    public StringProperty usernameProperty() {
        return username;
    }

    public void setUsername(String username) {
        this.username.set(username);
    }

    public String getEmail() {
        return email.get();
    }

    public StringProperty emailProperty() {
        return email;
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getPhoneNumber() {
        return phoneNumber.get();
    }

    public StringProperty phoneNumberProperty() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber.set(phoneNumber);
    }

    public String getUser_type() {
        return user_type.get();
    }

    public StringProperty user_typeProperty() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type.set(user_type);
    }

    public String getPassword() {
        return password.get();
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

}
