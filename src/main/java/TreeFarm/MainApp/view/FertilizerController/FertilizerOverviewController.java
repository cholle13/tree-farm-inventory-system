/**
 * author: Chris Holle
 */
package TreeFarm.MainApp.view.FertilizerController;

import TreeFarm.Database.SQLStuff;
import TreeFarm.Main.Manager;
import TreeFarm.MainApp.model.Fertilizer;
import TreeFarm.MainApp.ShowWindows;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.Formatter;

public class FertilizerOverviewController {
    @FXML
    private TableView<Fertilizer> fertilizerTable;
    @FXML
    private TableColumn<Fertilizer, String> fertNameColumn;
    @FXML
    private TableColumn<Fertilizer, Integer> fertSizeColumn;
    @FXML
    private Label nameLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private Label sizeLabel;
    @FXML
    private Label priceLabel;
    @FXML
    private Label quantityLable;

    private ShowWindows mainApp;

    public FertilizerOverviewController() {}

    @FXML
    private void initialize() {
        //init the tree table with two columns
        fertNameColumn.setCellValueFactory(fertNameColumn -> fertNameColumn.getValue().nameProperty());
        fertSizeColumn.setCellValueFactory(fertSizeColumn -> fertSizeColumn.getValue().sizeProperty().asObject());

        // Clear Tree details
        showFertilizerDetails(null);

        // Listen for selection changes and show person details when changed
        fertilizerTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showFertilizerDetails(newValue));
    }

    public void setMainApp(ShowWindows mainApp) {
        this.mainApp = mainApp;

        fertilizerTable.setItems(mainApp.getFertData());
    }

    private void showFertilizerDetails(Fertilizer fert) {
        if(fert != null){
            Formatter fmt = new Formatter();
            // Fill the labels with info from the fertilizer object
            nameLabel.setText(fert.getName());
            sizeLabel.setText(String.valueOf(fert.getSize()));
            descriptionLabel.setText(fert.getDescription());
            priceLabel.setText(String.valueOf(fmt.format("%.2f", fert.getPrice())));
            quantityLable.setText(String.valueOf(fert.getQuantity()));
        }else {
            // if none selected, remove text
            nameLabel.setText("");
            sizeLabel.setText("");
            descriptionLabel.setText("");
            priceLabel.setText("");
            quantityLable.setText("");
        }
    }

    private void handleNoSelectedFert(){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.initOwner(mainApp.getPrimaryStage());
        alert.setTitle("No Fertilizer Selected");
        alert.setHeaderText("Select a fertilizer");
        alert.setContentText("Please select a fertilizer.");
        alert.showAndWait();
    }

    // This deletes the tree altogether
    @FXML
    private void handleSellFert() {
        Fertilizer selectedFert = fertilizerTable.getSelectionModel().getSelectedItem();
        if(selectedFert == null){
            handleNoSelectedFert();
        }else{
            boolean okClicked = mainApp.showFertSellDialog(selectedFert);
            if(okClicked){
                SQLStuff.buyFertilizer(Manager.connection.getConnection(), selectedFert, selectedFert.getQuantity());
            }
            // Handles tables refresh
            initialize();
            showFertilizerDetails(selectedFert);
        }
    }

    // opens dialogue box and retrieves the edited fertilizer
    @FXML
    private void handleBuyFert() {
        Fertilizer selectedFert = fertilizerTable.getSelectionModel().getSelectedItem();
        if(selectedFert == null){
            handleNoSelectedFert();
        }else {
            boolean okClicked = mainApp.showFertBuyDialog(selectedFert);
            if(okClicked){
                SQLStuff.buyFertilizer(Manager.connection.getConnection(), selectedFert, selectedFert.getQuantity());
            }
            showFertilizerDetails(selectedFert);
        }
    }

    @FXML
    private void handleEditFert() {
        Fertilizer selectedFert = fertilizerTable.getSelectionModel().getSelectedItem();
        if(selectedFert == null){
            handleNoSelectedFert();
        }else{
            Fertilizer oldFert = new Fertilizer(selectedFert);
            boolean okClicked = mainApp.showFertEditDialog(selectedFert);
            if(okClicked){
                SQLStuff.editFertilizer(Manager.connection.getConnection(), selectedFert, oldFert);
                System.out.println("edited Ferts");
            }
            // Handles tables refresh
            initialize();
            showFertilizerDetails(selectedFert);
        }

    }
}
