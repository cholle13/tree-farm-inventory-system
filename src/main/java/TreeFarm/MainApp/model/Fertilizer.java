package TreeFarm.MainApp.model;

import javafx.beans.property.*;

public class Fertilizer {
    protected StringProperty name;
    protected IntegerProperty size;
    protected DoubleProperty price;
    protected StringProperty description;
    protected IntegerProperty quantity;

    public Fertilizer() {
        // ("Nitrogen", "Helps the leaves green out", 24, 2, 26.95)
        this.name = new SimpleStringProperty("Nitrogen");
        this.description = new SimpleStringProperty("Helps the leaves green out");
        this.size = new SimpleIntegerProperty(24);
        this.quantity = new SimpleIntegerProperty(2);
        this.price = new SimpleDoubleProperty(26.95);
    }

    public Fertilizer(String name, int size, double price,String description, int quantity) {
        this.name = new SimpleStringProperty(name);
        this.description = new SimpleStringProperty(description);
        this.size = new SimpleIntegerProperty(size);
        this.quantity = new SimpleIntegerProperty(quantity);
        this.price = new SimpleDoubleProperty(price);
    }

    public Fertilizer(Fertilizer that){
        this.name = new SimpleStringProperty(that.getName());
        this.description = new SimpleStringProperty(that.getDescription());
        this.size = new SimpleIntegerProperty(that.getSize());
        this.quantity = new SimpleIntegerProperty(that.getQuantity());
        this.price = new SimpleDoubleProperty(that.getPrice());
    }


    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public int getSize() {
        return size.get();
    }

    public IntegerProperty sizeProperty() {
        return size;
    }

    public void setSize(int size) {
        this.size.set(size);
    }

    public double getPrice() {
        return price.get();
    }

    public DoubleProperty priceProperty() {
        return price;
    }

    public void setPrice(double price) {
        this.price.set(price);
    }

    public String getDescription() {
        return description.get();
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public int getQuantity() {
        return quantity.get();
    }

    public IntegerProperty quantityProperty() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity.set(quantity);
    }
}
