\contentsline {section}{\numberline {1}General Information}{1}
\contentsline {subsection}{\numberline {1.1}Background Story}{1}
\contentsline {subsection}{\numberline {1.2}Outline}{1}
\contentsline {subsection}{\numberline {1.3}Roles}{1}
\contentsline {subsection}{\numberline {1.4}Business Rules}{1}
\contentsline {section}{\numberline {2}Requirements}{3}
\contentsline {subsection}{\numberline {2.1}Functional Requirements}{3}
\contentsline {subsection}{\numberline {2.2}Non-Functional Requirements}{3}
\contentsline {subsubsection}{\numberline {2.2.1}Reliability}{3}
\contentsline {subsubsection}{\numberline {2.2.2}Performance}{3}
\contentsline {subsubsection}{\numberline {2.2.3}Supportability}{3}
\contentsline {section}{\numberline {3}Analysis}{4}
\contentsline {subsection}{\numberline {3.1}Use Cases}{4}
\contentsline {subsubsection}{\numberline {3.1.1}Use Case 1}{4}
\contentsline {subsubsection}{\numberline {3.1.2}Use Case 2}{5}
\contentsline {subsubsection}{\numberline {3.1.3}Use Case 3}{6}
\contentsline {subsection}{\numberline {3.2}Domain Model}{7}
\contentsline {section}{\numberline {4}Design}{8}
\contentsline {subsection}{\numberline {4.1}Design Model}{8}
\contentsline {subsection}{\numberline {4.2}Operations Contracts}{9}
\contentsline {subsection}{\numberline {4.3}Sequence Diagrams}{10}
\contentsline {subsubsection}{\numberline {4.3.1}Use Case 1}{10}
\contentsline {subsubsection}{\numberline {4.3.2}Use Case 2}{11}
\contentsline {subsubsection}{\numberline {4.3.3}Use Case 3}{12}
\contentsline {subsection}{\numberline {4.4}Gantt Report}{13}
\contentsline {section}{\numberline {5}GRASPS and Design Patterns}{15}
\contentsline {subsection}{\numberline {5.1}Design Patterns}{15}
\contentsline {subsection}{\numberline {5.2}GRASPS}{15}
\contentsline {section}{\numberline {6}Evaluation/Testing}{16}
\contentsline {section}{\numberline {7}Conclusion}{16}
