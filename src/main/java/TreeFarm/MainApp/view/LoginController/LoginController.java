/**
 * author: Chris Holle
 */
package TreeFarm.MainApp.view.LoginController;

import TreeFarm.Database.SQLStuff;
import TreeFarm.Main.Manager;
import TreeFarm.MainApp.model.User;
import TreeFarm.MainApp.ShowWindows;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class LoginController {

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    private ShowWindows mainApp;
    private boolean okClicked = false;
    private User user;

    public void setMainApp(ShowWindows mainApp) {
        this.mainApp = mainApp;
    }

    public void initialize(){
        user = new User();
        user.setPassword("");
        user.setUsername("");
    }

    public LoginController() {}

    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void handleLogin() {

        if(isInputValid()){
            initialize();
            user.setUsername(username.getText());
            user.setPassword(password.getText());
            User newUser = SQLStuff.checkLoginUser(Manager.connection.getConnection(), user);
            if(newUser == null){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(mainApp.getPrimaryStage());
                alert.setTitle("Invalid Login!");
                alert.setHeaderText("Invalid username or password");
                alert.setContentText("Please contact system admin for proper credentials");
                alert.showAndWait();
            }else {
                // Saves user to MainApp to be used throughout the app
                mainApp.setLoggedInUser(newUser);

                if(newUser.getUser_type().equals("admin")){
                    mainApp.initRootLayout();
                }else {
                    mainApp.initEmployeeRootLayout();
                }
            }
            okClicked = true;
        }
    }

    private boolean isInputValid() {
        String errMssg = "";

        if(errMssg.length() == 0)
            return true;
        else{
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(mainApp.getPrimaryStage());
            alert.setTitle("Invalid Login!");
            alert.setHeaderText("Please correct the invalid fields");
            alert.setContentText(errMssg);

            alert.showAndWait();
            return false;
        }
    }

}
