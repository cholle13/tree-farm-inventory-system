/**
 * Author: Wyatt Snyder, Chris Holle
 */

package TreeFarm.Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Arrays;

public class ConnectionSingleton {
    private static final String DB_DRIVER = "org.apache.derby.jdbc.EmbeddedDriver";
    private static final String DB_CONNECTION = "jdbc:derby:TreeFarmDatabase;create=true";
    private static final String DB_USER = "";
    private static final String DB_PASSWORD = getDbConnectionPass();
    private static ConnectionSingleton dbConnection = null;
    private Connection connection;
    private ConnectionSingleton(){}

    private static String getDbConnectionPass(){
        char x[] = {'d','b','p','a', 's', 's', '1', '2', '3'};
        String b = "";
        for (int i = 0; i < x.length; i++) {
            x[i] += 10;
        }
        b = Arrays.toString(x);
        return b;
    }

    public static ConnectionSingleton getDBConnection() {
        if(dbConnection == null){
            synchronized (ConnectionSingleton.class){
                if(dbConnection == null){
                    dbConnection = new ConnectionSingleton();
                    try {
                        Class.forName(DB_DRIVER);
                    } catch (ClassNotFoundException e) {
                        System.out.println(e.getMessage());
                    }
                    try {
                        dbConnection.connection = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
                        return dbConnection;
                    } catch (SQLException e) {
                        System.out.println(e.getMessage());
                    }
                }
            }
        }

        return dbConnection;
    }

    public Connection getConnection() {
        return connection;
    }
}
