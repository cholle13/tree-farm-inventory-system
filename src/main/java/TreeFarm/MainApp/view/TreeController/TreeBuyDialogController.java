/**
 * author: Chris Holle
 */
package TreeFarm.MainApp.view.TreeController;

import TreeFarm.MainApp.model.Tree;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import javafx.scene.control.Spinner;

public class TreeBuyDialogController {

    @FXML
    private Spinner<Integer> buyTrees;

    private Stage dialogStage;
    private Tree tree;
    private boolean okClicked = false;

    @FXML
    private void initialize() {}

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setTree(Tree tr){
        this.tree = tr;
    }

    public boolean isOkClicked() {
        return okClicked;
    }

    @FXML
    private void handlBuyOk() {
        if(isInputValid()){
            int x = buyTrees.getValue();
            int y = this.tree.getQuantity();
            this.tree.setQuantity(y + x);
            okClicked = true;
            dialogStage.close();
        }
    }

    @FXML
    private void handlSellOk() {
        if(isInputValid()){
            int x = buyTrees.getValue();
            int y = this.tree.getQuantity();
            String errorMessage = "";
            if(y >= x){
                this.tree.setQuantity(y - x);
            }else{
                // Show the error message.
                errorMessage += "You do not have enough in stock to sell this number of trees!";
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(dialogStage);
                alert.setTitle("Not Enough Trees");
                alert.setHeaderText("Trying to sell more than you have in stock");
                alert.setContentText(errorMessage);

                alert.showAndWait();
            }


            okClicked = true;
            dialogStage.close();
        }
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    private boolean isInputValid() {
        String errorMessage = "";
        if(buyTrees.getValue() == null){
            errorMessage += "No values entered!\n";
        }
        // NEED TO FILTER DATA FOR ONLY INTEGERS
        if(errorMessage.length() == 0)
            return true;
        else{
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please correct invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}
