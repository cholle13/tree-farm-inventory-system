/**
 * author: Chris Holle
 */
package TreeFarm.MainApp.view.FertilizerController;

import TreeFarm.MainApp.model.Fertilizer;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Spinner;
import javafx.stage.Stage;

public class FertilizerBuyDialogController {

    @FXML
    private Spinner<Integer> buyFertilizer;


    private Stage dialogStage;
    private Fertilizer fertilizer;
    private boolean okClicked = false;

    @FXML
    private void initialize() {}

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setFertilizer(Fertilizer fert){
        this.fertilizer = fert;
    }

    public boolean isOkClicked() {
        return okClicked;
    }


    @FXML
    private void handlBuyOk() {
        if(isInputValid()){
            int x = buyFertilizer.getValue();
            int y = this.fertilizer.getQuantity();
            this.fertilizer.setQuantity(y + x);
            okClicked = true;
            dialogStage.close();
        }
    }

    @FXML
    private void handlSellOk() {
        if(isInputValid()){
            int x = buyFertilizer.getValue();
            int y = this.fertilizer.getQuantity();
            String errorMessage = "";
            if(y >= x){
                this.fertilizer.setQuantity(y - x);
            }else{
                // Show the error message.
                errorMessage += "You do not have enough in stock to sell this number of fertilizer!";
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.initOwner(dialogStage);
                alert.setTitle("Not Enough Fertilizer");
                alert.setHeaderText("Trying to sell more than you have in stock");
                alert.setContentText(errorMessage);

                alert.showAndWait();
            }

            okClicked = true;
            dialogStage.close();
        }
    }

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    private boolean isInputValid() {
        String errorMessage = "";
        if(buyFertilizer.getValue() == null){
            errorMessage += "No values entered!\n";
        }
        if(!(buyFertilizer.getValue() <= 100 && buyFertilizer.getValue() >= 0))
            errorMessage += "invalid input";

        // NEED TO FILTER DATA FOR ONLY INTEGERS
        if(errorMessage.length() == 0)
            return true;
        else{
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Number");
            alert.setHeaderText("Please fix invalid fields");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }
}
