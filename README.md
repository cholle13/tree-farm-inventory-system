# Tree Farm Inventory Management System  

## About  
This inventory management system will allow users to see inventory and delete/update items in inventory.  

The system will manage the following items
 - Trees
 - Fertilizer
 - Pottery

## What we will need for design

 - Class model
 - Sequence diagrams
 - Scenarios
 - 7 design patterns

## What we need for Documentation
 - Abstract 
 - Background story for introduction
 - Requirements 
 - Analysis, design 
 - Implementation
 - Evaluation/testing
 - Conclusion 
> must be written in latex

## Timeline

 - Documentation/Design by April 4th(get approved)
 - Implementation by April 20th
 - Testing By April 27th

## Roles
### Chris
 - 3 Design Patterns
 - Scenarios
 - Java UI

### Wyatt
- 3 Design Patterns
- Class Model
- Testing
> More to assign after first meeting

## Tech stack

 - Derby
 - Java SE
 - Git
 - Maven
 - JUnit5
 - JavaFX