package TreeFarm.MainApp.model;


import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class MarsFertilizer extends Fertilizer {
    public MarsFertilizer(){
        // ("Magnesium", "Helps the flowers", 16, 4, 20.95)
        this.name = new SimpleStringProperty("Magnesium");
        this.description = new SimpleStringProperty("Helps the flowers");
        this.size = new SimpleIntegerProperty(16);
        this.quantity = new SimpleIntegerProperty(4);
        this.price = new SimpleDoubleProperty(20.95);
    }}
