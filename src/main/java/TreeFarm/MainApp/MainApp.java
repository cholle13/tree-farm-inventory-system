/**
 * author: Chris Holle
 */

package TreeFarm.MainApp;

import TreeFarm.Main.Manager;
import javafx.application.Application;
import javafx.stage.Stage;

public class MainApp extends Application {

    @Override
    public void start(Stage primaryStage) {
        ShowWindows sw = new ShowWindows();
        Stage primaryStage1 = primaryStage;
        primaryStage1.setTitle("Mars Tree Farm");

        Manager.initDatabase();
        // Loads login screen and when user logs in the variable loggedInUser is saved
        sw.loginScreen(primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
