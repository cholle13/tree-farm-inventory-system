package TreeFarm.Main;

import TreeFarm.MainApp.model.Tree;

public interface AbstractTreeFactory {
    Tree makeTree(String type, int age, int size, double price, String name, String description, int quantity, String origin);
}
