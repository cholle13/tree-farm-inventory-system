/**
 * Author: Wyatt Snyder, Chris Holle
 */

package TreeFarm.Main;

import TreeFarm.Database.ConnectionSingleton;
import TreeFarm.Database.SQLStuff;
import TreeFarm.MainApp.model.*;
import javafx.collections.ObservableList;

import java.io.*;
import java.sql.Connection;
import java.util.HashSet;
import java.util.Set;

public class Manager {
    public static final ConnectionSingleton connection = ConnectionSingleton.getDBConnection();;
    public static void initDatabase() {
        TreeFactory factory = new TreeFactory();
        SQLStuff.createDBUsersTable(connection.getConnection());
        SQLStuff.createDBTreesTable(connection.getConnection());
        SQLStuff.createDBFertilizerTable(connection.getConnection());
        // only do if database is empty

        ObservableList<Tree> resultSet = SQLStuff.getTrees(connection.getConnection());
        if(resultSet == null){
            SQLStuff.insertIntoTreeTable(connection.getConnection(), factory.makeTree("Moniforous", 1, 5,
                    18.99, "Mars Pine", "Short tree with mars cones", 5, "Mars"));
            SQLStuff.insertIntoTreeTable(connection.getConnection(), factory.makeTree("Moniforous", 3, 15,
                    39.99, "Mars Pine", "Medium tree with mars cones", 2, "Mars"));
            SQLStuff.insertIntoTreeTable(connection.getConnection(), factory.makeTree("Moniforous", 9, 45,
                    124.99, "Mars Pine", "Tall tree with mars cones", 3, "Mars"));

            SQLStuff.insertIntoTreeTable(connection.getConnection(), factory.makeTree("Mevergreen", 1, 5,
                    18.99, "Mars Oak", "Short tree that is very hardy", 6, "Earth"));
            SQLStuff.insertIntoTreeTable(connection.getConnection(), factory.makeTree("Mevergreen", 3, 15,
                    39.99, "Mars Oak", "Medium tree that is very hardy", 7, "Earth"));
            SQLStuff.insertIntoTreeTable(connection.getConnection(), factory.makeTree("Mevergreen", 5, 45,
                    124.99, "Mars Oak", "Tall tree that is very hardy", 2, "Earth"));

            SQLStuff.insertIntoTreeTable(connection.getConnection(), factory.makeTree("Meciduous", 6, 5,
                    18.99, "Mars Maple", "Short tree with pretty fall colors", 8, "Venus"));
            SQLStuff.insertIntoTreeTable(connection.getConnection(), factory.makeTree("Meciduous", 6, 15,
                    25.99, "Mars Maple", "Medium tree with pretty fall colors", 8, "Venus"));
            SQLStuff.insertIntoTreeTable(connection.getConnection(), factory.makeTree("Meciduous", 6, 45,
                    45.99, "Mars Maple", "Tall tree with pretty fall colors", 8, "Venus"));
        }

        ObservableList<User> resultSet2 = SQLStuff.getUsers(connection.getConnection());
        if(resultSet2 == null){
            SQLStuff.createUser(connection.getConnection(), new User("test", "guy", "testDude12",
                    "test@gmail.com", "123-456-4789", "employee", "pass567"));

            SQLStuff.createUser(connection.getConnection(), new User("Chris", "Holle", "cholle",
                    "chrisaholle@gmail.com", "979-451-5466", "admin", "pass123"));
            SQLStuff.createUser(connection.getConnection(), new User("Tomas", "Cerny", "tomas_cerny",
                    "tomas_cerny@baylor.edu", "123-345-6789", "admin", "Pass123"));
        }

        ObservableList<Fertilizer> resultSet3 = SQLStuff.getFertilizer(connection.getConnection());
        if(resultSet3 == null){
            SQLStuff.insertIntoFertilizerTable(connection.getConnection(), makeFertilizer("Standard"));
            SQLStuff.insertIntoFertilizerTable(connection.getConnection(), makeFertilizer("Earth"));
            SQLStuff.insertIntoFertilizerTable(connection.getConnection(), makeFertilizer("Mars"));
        }
    }

    public static void importUsers(File FileName, Connection dbConnection) {
        BufferedReader reader;
        String line;
        UserBuilder builder = new UserBuilder();
        Set<String> userNames = new HashSet<>();
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(FileName), "UTF-8"));
            while((line = reader.readLine()) != null){
                String[] values = line.split(",");
                if(!userNames.contains(values[2])){
                    userNames.add(values[2]);
                    createUser(builder, values);
                    User user = builder.getUser();
                    SQLStuff.createUser(dbConnection, user);
                }
            }
            reader.close();
        } catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    private static void createUser(UserBuilder builder, String[] values){
        builder.buildUser(values[0], values[1], values[2], values[3], values[4], values[5], values[6]);
    }

    private static Fertilizer makeFertilizer(String type){
        switch (type) {
            case "Mars":
                return new MarsFertilizer();
            case "Earth":
                return new EarthFertilizer();
            default:
                return new Fertilizer();
        }
    }
}
