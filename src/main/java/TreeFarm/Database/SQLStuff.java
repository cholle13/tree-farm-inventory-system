/**
 * Author: Wyatt Snyder, Chris Holle
 */

package TreeFarm.Database;

import TreeFarm.Main.TreeFactory;
import TreeFarm.MainApp.model.Fertilizer;
import TreeFarm.MainApp.model.Tree;
import TreeFarm.MainApp.model.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

public class SQLStuff {
    private static ResultSet executeSQL(Connection dbConnection, String sql) {
        Statement statement = null;
        ResultSet rs = null;
        try {
            statement = dbConnection.createStatement();
            // execute the SQL statement
            statement.execute(sql);
            rs = statement.getResultSet();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }finally {
            try {
                if(statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(rs != null)
            return rs;
        else
            return null;
    }

    public static void createDBTreesTable(Connection dbConnection) {
        String createTableSQL = "CREATE TABLE  Trees("
                + "TreeType VARCHAR(20) NOT NULL, " + "TreeAge INTEGER NOT NULL, TreeSize INTEGER NOT NULL, "
                + "Price FLOAT NOT NULL, TreeName VARCHAR(30) NOT NULL, Description VARCHAR(50) NOT NULL, Quantity INTEGER NOT NULL, TreeOrigin VARCHAR(30))";

        executeSQL(dbConnection, createTableSQL);
    }

    public static void createDBFertilizerTable(Connection dbConnection) {
        String createTableSQL = "CREATE TABLE  Fertilizer(" + "Name VARCHAR(20) NOT NULL, "
                + "FertilizerSize INT NOT NULL, Price FLOAT NOT NULL, Description VARCHAR(50) NOT NULL, Quantity INTEGER NOT NULL)";

        executeSQL(dbConnection, createTableSQL);
    }

    public static void insertIntoTreeTable(Connection dbConnection, Tree tree) {
        PreparedStatement stmt = null;
        try {
            stmt = dbConnection.prepareStatement("INSERT INTO Trees(TreeType, TreeAge, TreeSize, Price, "
                    + "TreeName, Description, Quantity, TreeOrigin) VALUES(?,?,?,?,?,?,?,?)");
            stmt.setString(1, tree.getType());
            stmt.setInt(2, tree.getAge());
            stmt.setInt(3, tree.getSize());
            stmt.setFloat(4, (float) tree.getPrice());
            stmt.setString(5, tree.getName());
            stmt.setString(6, tree.getDescription());
            stmt.setInt(7, tree.getQuantity());
            stmt.setString(8, tree.getOrigin());
            stmt.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void editTree(Connection dbConnection, Tree tree, Tree oldTree){
        PreparedStatement stmt = null;
        try{
            stmt = dbConnection.prepareStatement("UPDATE Trees SET Treename = ?, TreeType = ?, Treesize = ?, Price = ?, "
                    + "Description = ? WHERE TreeName = ? AND Treesize = ?");
            stmt.setString(1, tree.getName());
            stmt.setString(2, tree.getType());
            stmt.setInt(3, tree.getSize());
            stmt.setFloat(4, (float) tree.getPrice());
            stmt.setString(5, tree.getDescription());
            stmt.setString(6, oldTree.getName());
            stmt.setInt(7, oldTree.getSize());
            stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public static void insertIntoFertilizerTable(Connection dbConnection, Fertilizer fertilizer) {
        PreparedStatement stmt = null;
        try {
            stmt = dbConnection.prepareStatement("INSERT INTO Fertilizer(Name, FertilizerSize, Price, Description, Quantity)"
                    + " VALUES(?,?,?,?,?)");
            stmt.setString(1, fertilizer.getName());
            stmt.setInt(2, fertilizer.getSize());
            stmt.setFloat(3, (float) fertilizer.getPrice());
            stmt.setString(4, fertilizer.getDescription());
            stmt.setInt(5, fertilizer.getQuantity());
            stmt.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void editFertilizer(Connection dbConnection, Fertilizer fertilizer, Fertilizer oldFertilizer) {
        PreparedStatement stmt = null;
        try {
            stmt = dbConnection.prepareStatement("UPDATE Fertilizer SET Name = ?, FertilizerSize = ?,"
                    + " Price = ?, Description = ? WHERE Name = ? AND FertilizerSize = ?");
            stmt.setString(1, fertilizer.getName());
            stmt.setInt(2, fertilizer.getSize());
            stmt.setFloat(3, (float) fertilizer.getPrice());
            stmt.setString(4, fertilizer.getDescription());
            stmt.setString(5, oldFertilizer.getName());
            stmt.setInt(6, oldFertilizer.getSize());
            stmt.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static ObservableList<Tree> getTrees(Connection dbConnection){
        String getTreesSQL = "SELECT * FROM Trees";
        Statement stmt = null;
        TreeFactory factory = new TreeFactory();
        ObservableList<Tree> treeList = FXCollections.observableArrayList();
        ResultSet rs = null;
        Tree x;
        try{
            stmt = dbConnection.createStatement();
            stmt.execute(getTreesSQL);
            rs = stmt.getResultSet();
            while(rs != null && rs.next()){
                x = factory.makeTree(rs.getString("TreeType"), rs.getInt("TreeAge"),
                        rs.getInt("TreeSize"), rs.getFloat("Price"), rs.getString("TreeName"),
                        rs.getString("Description"), rs.getInt("Quantity"), rs.getString("TreeOrigin"));

                treeList.add(x);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
                if(rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return treeList;
    }

    public static ObservableList<Fertilizer> getFertilizer(Connection dbConnection){
        String getFertilizerSQL = "SELECT * FROM Fertilizer";
        Statement stmt = null;
        ObservableList<Fertilizer> fertList = FXCollections.observableArrayList();
        ResultSet rs = null;
        Fertilizer x;
        try{
            stmt = dbConnection.createStatement();
            stmt.execute(getFertilizerSQL);
            rs = stmt.getResultSet();
            while(rs != null && rs.next()){
                x = new Fertilizer(rs.getString("Name"), rs.getInt("Fertilizersize"),
                        rs.getDouble("Price"), rs.getString("Description"), rs.getInt("Quantity"));
                fertList.add(x);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
                if(rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return fertList;
    }

    public static ObservableList<User> getUsers(Connection dbConnection) {
        Statement stmt = null;
        ObservableList<User> userList = FXCollections.observableArrayList();
        String getUsersSQL = "SELECT * FROM Users";
        ResultSet rs = null;
        User x;
        try{
            stmt = dbConnection.createStatement();
            stmt.execute(getUsersSQL);
            rs = stmt.getResultSet();
            while(rs.next()) {
                x = new User(rs.getString("firstName"), rs.getString("lastName"),
                        rs.getString("username"), rs.getString("email"), rs.getString("Phonenumber"),
                        rs.getString("Type"), rs.getString("password"));
                userList.add(x);
            }
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
                if(rs != null)
                    rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return userList;
    }

    public static void buyFertilizer(Connection dbConnection, Fertilizer fertilizer, int quantity) {
        PreparedStatement stmt = null;
        try {
            stmt = dbConnection.prepareStatement("UPDATE Fertilizer SET Quantity = ? WHERE Name = ? AND FertilizerSize = ?");
            stmt.setInt(1, quantity);
            stmt.setString(2, fertilizer.getName());
            stmt.setInt(3, fertilizer.getSize());
            stmt.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void buyTrees(Connection dbConnection, Tree tree, int quantity) {
        PreparedStatement stmt = null;
        try {
            stmt = dbConnection.prepareStatement("UPDATE Trees SET Quantity = ? WHERE TreeName = ? AND TreeSize = ?");
            stmt.setInt(1, quantity);
            stmt.setString(2, tree.getName());
            stmt.setInt(3, tree.getSize());
            stmt.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void dropDBTable(Connection dbConnection, String tableName) {
        String dropTableSQL = "DROP TABLE " + tableName;

        executeSQL(dbConnection, dropTableSQL);
    }

    public static void createDBUsersTable(Connection dbConnection) {
        String createTableSQL = "CREATE TABLE  Users("
                + "firstName VARCHAR(30) NOT NULL, " + "lastName VARCHAR(30) NOT NULL, username VARCHAR(30) NOT NULL, "
                + "email VARCHAR(30) NOT NULL, phoneNumber VARCHAR(30) NOT NULL, Type VARCHAR(30) NOT NULL, password VARCHAR(30) NOT NULL)";

        executeSQL(dbConnection, createTableSQL);
    }

    public static void createUser(Connection dbConnection, User user) {
        PreparedStatement stmt = null;
        try {
            stmt = dbConnection.prepareStatement("INSERT INTO Users(FirstName, LastName, Username, Email, "
                    + "PhoneNumber, Type, Password) VALUES(?,?,?,?,?,?,?)");
            stmt.setString(1, user.getFirstName());
            stmt.setString(2, user.getLastName());
            stmt.setString(3, user.getUsername());
            stmt.setString(4, user.getEmail());
            stmt.setString(5, user.getPhoneNumber());
            stmt.setString(6, user.getUser_type());
            stmt.setString(7, user.getPassword());
            stmt.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public static void deleteUser(Connection dbConnection, User user) {
        PreparedStatement stmt = null;
        try {
            stmt = dbConnection.prepareStatement("DELETE FROM Users WHERE username = ?");
            stmt.setString(1, user.getUsername());
            stmt.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public static void editUser(Connection dbConnection, User user, User oldUser) {
        PreparedStatement stmt = null;
        try {
            stmt = dbConnection.prepareStatement("UPDATE Users SET FirstName = ?, LastName = ?, Username = ?, "
                    + "Email = ?, Phonenumber = ? WHERE Username = ?");
            stmt.setString(1, user.getFirstName());
            stmt.setString(2, user.getLastName());
            stmt.setString(3, user.getUsername());
            stmt.setString(4, user.getEmail());
            stmt.setString(5, user.getPhoneNumber());
            stmt.setString(6, oldUser.getUsername());
            stmt.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static User checkLoginUser(Connection dbConnection, User user) {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        User tempUser = null;
        try {
            stmt = dbConnection.prepareStatement("SELECT * FROM Users WHERE Username = ? AND Password = ?");
            stmt.setString(1, user.getUsername());
            stmt.setString(2, user.getPassword());
            rs = stmt.executeQuery();
            if(rs.next())
                tempUser = new User(rs.getString("firstName"), rs.getString("lastName"),
                    rs.getString("username"), rs.getString("email"), rs.getString("Phonenumber"),
                    rs.getString("Type"), rs.getString("password"));
        }catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if(stmt != null)
                    stmt.close();
                if(rs != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(tempUser != null)
            return tempUser;
        else
            return null;
    }
}
