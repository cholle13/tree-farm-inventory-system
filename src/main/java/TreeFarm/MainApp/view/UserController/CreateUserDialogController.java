/**
 * author: Chris Holle
 */
package TreeFarm.MainApp.view.UserController;

import TreeFarm.MainApp.model.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class CreateUserDialogController {
    @FXML
    private TextField firstName;
    @FXML
    private TextField lastName;
    @FXML
    private TextField username;
    @FXML
    private TextField email;
    @FXML
    private TextField phoneNumber;
    @FXML
    private PasswordField password;
    @FXML
    private ChoiceBox<String> choiceBox;

    private Stage dialogStage;
    private User user;
    private boolean okClicked = false;

    /**
     * isOkClicked
     * Sends data back to alert Main window of submission
     * @return
     */
    public boolean isOkClicked() {
        return okClicked;
    }

    /**
     * setDialogStage(Stage)
     * Initializes create user dialog window
     * @param dialogStage
     */
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    /**
     * initialize()
     * Inits up data for Create user window
     */
    public void initialize(){
        ObservableList<String> list = FXCollections.observableArrayList();
        list.addAll("employee", "admin");
        //populate the Choicebox;
        choiceBox.setItems(list);
        user = new User();
    }

    /**
     * setUser
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * handleCancel()
     * Handles the cancel button
     */
    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    /**
     * handleOk()
     * Creates a handler to save data on button pressed
     */
    @FXML
    private void handleOk() {
        if(isInputValid()){
            user.setFirstName(firstName.getText());
            user.setLastName(lastName.getText());
            user.setUsername(username.getText());
            user.setEmail(email.getText());
            user.setPhoneNumber(phoneNumber.getText());
            user.setUser_type(choiceBox.getValue());
            user.setPassword(password.getText());

            okClicked = true;
            dialogStage.close();

        }
    }

    /**
     * isInputValid()
     * Handles data filtering
     * @return
     */
    private boolean isInputValid() {
        String errMssg = "";
        if(firstName.getText() == null || firstName.getText().length() == 0)
            errMssg += "No first name entered!\n";
        if(!(firstName.getText().matches("[a-zA-Z]+")))
            errMssg += "First Name field should only contain alpha characters\n";

        if(lastName.getText() == null || lastName.getText().length() == 0)
            errMssg += "No last name entered!\n";
        if(!(lastName.getText().matches("[a-zA-Z]+")))
            errMssg += "Last Name field should only contain alpha characters\n";

        if(username.getText() == null || username.getText().length() == 0)
            errMssg += "No username entered!\n";
        if(!(username.getText().matches("[a-z._A-Z0-9]+")))
            errMssg += "username field should only contain alpha characters, numerical digits, a period or _\n";

        if(email.getText() == null || email.getText().length() == 0)
            errMssg += "No email entered!\n";
        if(!(email.getText().matches("[a-z@.A-Z]+")))
            errMssg += "email field should only contain alpha characters, @ and a .\n";

        if(phoneNumber.getText() == null || phoneNumber.getText().length() == 0)
            errMssg += "No phone-number entered!\n";
        if(!(phoneNumber.getText().matches("[0-9]{3}[-]{1}[0-9]{3}[-]{1}[0-9]{4}")))
            errMssg += "Phone number field should be in the format ###-###-####\n";

        if(choiceBox.getValue() == null || choiceBox.getValue().length() == 0){
            errMssg += "No user type entered!\n";
        }

        if(errMssg.length() == 0)
            return true;
        else{
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields");
            alert.setHeaderText("Please fix invalid fields");
            alert.setContentText(errMssg);

            alert.showAndWait();
            return false;
        }
    }

}
