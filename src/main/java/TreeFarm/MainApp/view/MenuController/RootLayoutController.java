/**
 * author: Chris Holle
 */
package TreeFarm.MainApp.view.MenuController;

import TreeFarm.MainApp.model.User;
import TreeFarm.MainApp.ShowWindows;
import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class RootLayoutController {

    @FXML
    private Label welcomeLabel;

    private User loggedInUser;

    private ShowWindows mainApp;

    public void setLoggedInUser(User user){
        loggedInUser = user;
        welcomeLabel.setText("Welcome " + loggedInUser.getFirstName() + " " + loggedInUser.getLastName() + "!");
    }

    public void setMainApp(ShowWindows mainApp){
        this.mainApp = mainApp;
    }

    @FXML
    private void handleManageTreeOverview() {
        mainApp.showTreeoverview();
    }


    @FXML
    private void handleManageUsersOverview() {
        mainApp.showUserOverview();
    }
    @FXML
    private void handleManageFertilizerOverview() {
        mainApp.showFertoverview();
    }

    @FXML
    private void handleImportUsers() {
        mainApp.setLoggedInUser(null);
        mainApp.showImportUsers();
    }

    @FXML
    private void handleLogout() {
        mainApp.loginScreen(mainApp.getPrimaryStage());
    }

    @FXML
    private void handleExit() {
        System.exit(0);
    }

}
