/**
 * author: Chris Holle
 */
package TreeFarm.MainApp.view.TreeController;

import TreeFarm.MainApp.model.Tree;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.Formatter;

public class EditTreeDialogController {
    @FXML
    private TextField typeNameLabel;
    @FXML
    private TextField treeNameLabel;
    @FXML
    private TextField sizeLabel;
    @FXML
    private TextArea descriptionLabel;
    @FXML
    private TextField priceLabel;

    private Stage dialogStage;
    private Tree tree;
    private boolean okClicked = false;

    public boolean isOkClicked() {
        return okClicked;
    }
    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void setTree(Tree tree){
        Formatter fmt = new Formatter();
        this.tree = tree;
        typeNameLabel.setText(tree.getType());
        treeNameLabel.setText(tree.getName());
        sizeLabel.setText(String.valueOf(tree.getSize()));
        priceLabel.setText(String.valueOf(fmt.format("%.2f", tree.getPrice())));
        descriptionLabel.setText(tree.getDescription());
    }

    public void initialize(){}

    @FXML
    private void handleCancel() {
        dialogStage.close();
    }

    @FXML
    private void handleOk() {
        if(isInputValid()){
            tree.setName(treeNameLabel.getText());
            tree.setType(typeNameLabel.getText());
            tree.setSize(Integer.parseInt(sizeLabel.getText()));
            tree.setPrice(Double.parseDouble(priceLabel.getText()));
            tree.setDescription(descriptionLabel.getText());

            okClicked = true;
            dialogStage.close();
            initialize();

        }
    }

    private boolean isInputValid() {
        String errMssg = "";
        if(treeNameLabel.getText() == null || treeNameLabel.getText().length() == 0){
            errMssg += "No tree name entered!\n";
        }
        if(!(treeNameLabel.getText().matches("[a-z A-Z]+")))
            errMssg += "Name field should only contain alpha characters!\n";
        if(typeNameLabel.getText() == null || typeNameLabel.getText().length() == 0){
            errMssg += "No tree type entered!\n";
        }
        if(!(typeNameLabel.getText().matches("[a-z A-Z]+")))
            errMssg += "Tree Type field should only contain alpha characters!\n";

        if(descriptionLabel.getText() == null || descriptionLabel.getText().length() == 0){
            errMssg += "No description entered!\n";
        }
        if(!(descriptionLabel.getText().matches("[a-z A-Z.!,?]+")))
            errMssg += "Description field should only contain alpha characters!\n";
        if(sizeLabel.getText() == null || sizeLabel.getText().length() == 0){
            errMssg += "No size entered!\n";
        }
        if(!(sizeLabel.getText().matches("[0-9]+")))
            errMssg += "Size field should only contain numerical digits!\n";
        if(priceLabel.getText() == null || priceLabel.getText().length() == 0){
            errMssg += "No price entered!\n";
        }
        if(!(priceLabel.getText().matches("[0-9]+[.]{1}[0-9]+")))
            errMssg += "Name field should only contain numerical digits and a period!\n";

        if(errMssg.length() == 0)
            return true;
        else{
            // Show the error message.
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(dialogStage);
            alert.setTitle("Invalid Fields!");
            alert.setHeaderText("Please fix the invalid fields");
            alert.setContentText(errMssg);

            alert.showAndWait();
            return false;
        }
    }
}
