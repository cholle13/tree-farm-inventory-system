package TreeFarm.Main;

import TreeFarm.MainApp.model.Tree;

public class TreeFactory implements AbstractTreeFactory{

    public Tree makeTree(String type, int age, int size, double price, String name, String description, int quantity, String origin){
        return new Tree(type, age, size,  price, name, description, quantity, origin);
    }
}
