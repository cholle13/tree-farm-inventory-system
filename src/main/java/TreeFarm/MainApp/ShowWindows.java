package TreeFarm.MainApp;

import TreeFarm.Database.SQLStuff;
import TreeFarm.Main.Manager;
import TreeFarm.MainApp.model.Fertilizer;
import TreeFarm.MainApp.model.Tree;
import TreeFarm.MainApp.model.User;
import TreeFarm.MainApp.view.FertilizerController.FertilizerBuyDialogController;
import TreeFarm.MainApp.view.FertilizerController.FertilizerEditDialogController;
import TreeFarm.MainApp.view.FertilizerController.FertilizerOverviewController;
import TreeFarm.MainApp.view.LoginController.LoginController;
import TreeFarm.MainApp.view.MenuController.ImportUsersController;
import TreeFarm.MainApp.view.MenuController.RootLayoutController;
import TreeFarm.MainApp.view.TreeController.EditTreeDialogController;
import TreeFarm.MainApp.view.TreeController.TreeBuyDialogController;
import TreeFarm.MainApp.view.TreeController.TreeOverviewController;
import TreeFarm.MainApp.view.UserController.CreateUserDialogController;
import TreeFarm.MainApp.view.UserController.EditUserDialogController;
import TreeFarm.MainApp.view.UserController.UserOverviewController;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;


public class ShowWindows {
    private Stage primaryStage;
    private BorderPane rootLayout;
    private User loggedInUser;

    public void setLoggedInUser(User user){ loggedInUser = user; }

    public ObservableList<Tree> getTreeData() {
        return SQLStuff.getTrees(Manager.connection.getConnection());
    }

    public ObservableList<User> getUserData() {
        return SQLStuff.getUsers(Manager.connection.getConnection());
    }

    public ObservableList<Fertilizer> getFertData() { return SQLStuff.getFertilizer(Manager.connection.getConnection()); }

    public void loginScreen(Stage pStage){
        this.primaryStage = pStage;
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/LoginController/UserLogin.fxml"));
            AnchorPane rootLogin = loader.load();
            Scene scene = new Scene(rootLogin);
            primaryStage.setScene(scene);

            // Set the tree into the controller
            LoginController controller = loader.getController();
            controller.setMainApp(this);

            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/MenuController/MenuWrapper.fxml"));
            rootLayout = loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);

            primaryStage.setScene(scene);

            RootLayoutController controller = loader.getController();
            controller.setLoggedInUser(loggedInUser);
            controller.setMainApp(this);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initEmployeeRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/MenuController/EmployeeMenuWrapper.fxml"));
            rootLayout = loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);

            primaryStage.setScene(scene);

            RootLayoutController controller = loader.getController();
            controller.setLoggedInUser(loggedInUser);
            controller.setMainApp(this);

            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showTreeoverview(){
        try{
            // load tree overview
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/TreeController/ManageTrees.fxml"));
            AnchorPane tableOverview = loader.load();
            rootLayout.setCenter(tableOverview);

            TreeOverviewController controller = loader.getController();
            controller.setMainApp((this));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showUserOverview(){
        try {
            // load user overview
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/UserController/ManageUsers.fxml"));
            Pane tableOverview = loader.load();
            rootLayout.setCenter(tableOverview);

            UserOverviewController controller = loader.getController();
            controller.setMainApp((this));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public boolean showTreeBuyDialog(Tree tree){
        try{
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/TreeController/BuyTrees.fxml"));
            Pane page = loader.load();

            // Create the dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Buy Trees");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the tree into the controller
            TreeBuyDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setTree(tree);

            // Show the dialog and wait for user to close
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch(IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showTreeSellDialog(Tree tree){
        try{
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/TreeController/SellTrees.fxml"));
            Pane page = loader.load();

            // Create the dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Sell Trees");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the tree into the controller
            TreeBuyDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setTree(tree);

            // Show the dialog and wait for user to close
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch(IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showTreeEditDialog(Tree tree){
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/TreeController/EditTree.fxml"));
            Pane page = loader.load();

            // Create the dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Tree");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the tree into the controller
            EditTreeDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setTree(tree);

            // Show the dialog and wait for user to close
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showUserEditDialog(User user) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/UserController/EditUser.fxml"));
            Pane page = loader.load();

            // Create the dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit User");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the tree into the controller
            EditUserDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setUser(user);

            // Show the dialog and wait for user to close
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }


    public boolean showCreateUserDialog(User user) {
        try {
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/UserController/CreateUser.fxml"));
            Pane page = loader.load();

            // Create the dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Create User");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the user into the controller
            CreateUserDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setUser(user);

            // Show the dialog and wait for user to close
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void showFertoverview(){
        try{
            // load fertilizer overview
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/FertilizerController/ManageFertilizer.fxml"));
            AnchorPane tableOverview = loader.load();
            rootLayout.setCenter(tableOverview);

            FertilizerOverviewController controller = loader.getController();
            controller.setMainApp((this));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean showFertBuyDialog(Fertilizer fert){
        try{
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/FertilizerController/BuyFertilizer.fxml"));
            Pane page = loader.load();

            // Create the dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Buy Fertilizer");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the tree into the controller
            FertilizerBuyDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setFertilizer(fert);

            // Show the dialog and wait for user to close
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch(IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showFertSellDialog(Fertilizer fert){
        try{
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/FertilizerController/SellFertilizer.fxml"));
            Pane page = loader.load();

            // Create the dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Sell Fertilizer");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the tree into the controller
            FertilizerBuyDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setFertilizer(fert);

            // Show the dialog and wait for user to close
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch(IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showFertEditDialog(Fertilizer fert){
        try{
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/FertilizerController/EditFertilizer.fxml"));
            Pane page = loader.load();

            // Create the dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Edit Fertilizer");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the tree into the controller
            FertilizerEditDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setFertilizer(fert);

            // Show the dialog and wait for user to close
            dialogStage.showAndWait();

            return controller.isOkClicked();
        } catch(IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void showImportUsers(){
        try{
            // Load the fxml file and create a new stage for the popup dialog.
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/MenuController/ImportUsers.fxml"));
            Pane page = loader.load();

            // Create the dialog stage
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Import Users");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Set the tree into the controller
            ImportUsersController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.setMainApp(this);

            // Show the dialog and wait for user to close
            dialogStage.showAndWait();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    public Stage getPrimaryStage(){
        return primaryStage;
    }
}
