/**
 * author: Chris Holle
 */
package TreeFarm.MainApp.view.UserController;

import TreeFarm.Database.SQLStuff;
import TreeFarm.Main.Manager;
import TreeFarm.MainApp.model.User;
import TreeFarm.MainApp.ShowWindows;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class UserOverviewController {
        @FXML
        private TableView<User> userTable;
        @FXML
        private TableColumn<User, String> firstNameColumn;
        @FXML
        private TableColumn<User, String> lastNameColumn;
        @FXML
        private Label firstName;
        @FXML
        private Label lastName;
        @FXML
        private Label username;
        @FXML
        private Label email;
        @FXML
        private Label phoneNumber;
        @FXML
        private Label user_type;
        @FXML
        private Label password;

        private ShowWindows mainApp;

        public UserOverviewController() {}

        @FXML
        private void initialize() {
            //init the user table with two columns
            firstNameColumn.setCellValueFactory(firstNameColumn -> firstNameColumn.getValue().firstNameProperty());
            lastNameColumn.setCellValueFactory(lastNameColumn -> lastNameColumn.getValue().lastNameProperty());

            // Clear User details
            showUserDetails(null);

            // Listen for selection changes and show person details when changed
            userTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> showUserDetails(newValue));
        }

    /**
     * setMainApp
     * initializes the MainApp Window
     * @param mainApp
     */
    public void setMainApp(ShowWindows mainApp) {
            this.mainApp = mainApp;
            userTable.setItems(mainApp.getUserData());
    }

    /**
     * showUserDetails
     * fills in user data on Manage User window
     * @param user
     */
    private void showUserDetails(User user) {
        if(user != null){
            // Fill the labels with info from the user object
            firstName.setText(user.getFirstName());
            lastName.setText(user.getLastName());
            username.setText(String.valueOf(user.getUsername()));
            email.setText(String.valueOf(user.getEmail()));
            phoneNumber.setText(user.getPhoneNumber());
            user_type.setText(String.valueOf(user.getUser_type()));
            password.setText(String.valueOf(user.getPassword()));

        }else {
            // if none selected, remove text
            firstName.setText("");
            lastName.setText("");
            username.setText("");
            phoneNumber.setText("");
            email.setText("");
            user_type.setText("");
            password.setText("");
        }
    }

    /**
     * handleCreateUser
     * Spawns a Create user window and observes it for changes
     */
    @FXML
    private void handleCreateUser() {
        User tempUser = new User();
        boolean okClicked = mainApp.showCreateUserDialog(tempUser);
        boolean inDatabase = false;
        if(okClicked){
            ObservableList<User> myUserData = mainApp.getUserData();
            for(User myUser : myUserData){
                if(myUser.getUsername().equals(tempUser.getUsername())){
                    inDatabase = true;
                }
            }
            if(!inDatabase){
                SQLStuff.createUser(Manager.connection.getConnection(), tempUser);
            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.initOwner(mainApp.getPrimaryStage());
                alert.setTitle("User in Database");
                alert.setHeaderText("The User already exist");
                alert.setContentText("Please choose a different username.");
                alert.showAndWait();
            }
            // Handles tables refresh to pull data
            setMainApp(mainApp);
            initialize();
            showUserDetails(tempUser);
        }
    }

    /**
     * handleNoUserSelected
     * Spawns alert window saying that no user is selected
     */
    private void handleNoUserSelected() {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.initOwner(mainApp.getPrimaryStage());
        alert.setTitle("No Selection");
        alert.setHeaderText("No User Selected");
        alert.setContentText("Please select a user in the table.");
        alert.showAndWait();
    }

    /**
     * handleDeleteUser()
     * Deletes selected user
     */
    @FXML
    private void handleDeleteUser() {
        User user = userTable.getSelectionModel().getSelectedItem();
        int selectedIndex = userTable.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            if(user != null) {
                SQLStuff.deleteUser(Manager.connection.getConnection(), user);
                userTable.getItems().remove(selectedIndex);
            }
        } else {
            // Nothing selected
            handleNoUserSelected();
        }
    }

    /**
     * handleEditUser()
     * Spawns Edit user window with user data filled in for editing
     * Observes widow and changes main upon change
     */
    @FXML
    private void handleEditUser() {
        User selectedUser = userTable.getSelectionModel().getSelectedItem();
        User oldUser;
        if(selectedUser == null){
            handleNoUserSelected();
        } else {
            oldUser = new User(selectedUser);
            boolean inDatabase = false;
            ObservableList<User> myUserData = mainApp.getUserData();
            boolean okClicked = mainApp.showUserEditDialog(selectedUser);
            if (okClicked) {
                for (User myUser : myUserData) {
                    if (myUser.getUsername().equals(selectedUser.getUsername()) && !(selectedUser.getUsername().equals(oldUser.getUsername()))) {
                        inDatabase = true;
                    }
                }
                if (!inDatabase) {
                    SQLStuff.editUser(Manager.connection.getConnection(), selectedUser, oldUser);
                } else {
                    // Nothing Selected
                    Alert alert = new Alert(Alert.AlertType.WARNING);
                    alert.initOwner(mainApp.getPrimaryStage());
                    alert.setTitle("User in database");
                    alert.setHeaderText("The User already exist");
                    alert.setContentText("Please select a different username!");
                    alert.showAndWait();
                }
            }
            setMainApp(mainApp);
            initialize();
            showUserDetails(selectedUser);
        }
    }
}
